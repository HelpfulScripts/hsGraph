hsGraph 
========
[![npm version](https://badge.fury.io/js/hsgraph.svg)](https://badge.fury.io/js/hsgraph) 
[![Build status](https://ci.appveyor.com/api/projects/status/g140xuxrfnc46iwl?svg=true)](https://ci.appveyor.com/project/HelpfulScripts/hsgraph)
[![Built with Grunt](https://cdn.gruntjs.com/builtwith.svg)](https://gruntjs.com/) 
[![NPM License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://www.npmjs.com/package/hsgraph)

Helpful Scripts grpah plotting functions.

**hsGraph** Provides JavaScript directives to facilitate plotting data series using the mithril framework.

## Installation
`npm i hsgraph`


