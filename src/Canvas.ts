/**
 * # Canvas
 * renders the graph's background.
 * 
 * ### Attributes
 * The `Canvas` class is called by {@link Graph.Graph `Graph`} as 
 * `m(Canvas, { cfg:cfg.canvas}))`
 * with the following attributes:
 * - cfg: a {@link Canvas.CanvasConfig `CanvasConfig`} configuration object
 * 
 * ### Configurations and Defaults
 * See {@link Canvas.Canvas.defaultConfig Canvas.defaultConfig}
 */

/** */
import { m, Vnode}              from 'hsutil/mithril';
import { SVGElem, Area }        from './SVGElem';
import { Config, VisibleCfg }   from './Graph';


/** Defines configurable settings. */
export interface CanvasConfig extends VisibleCfg{
    range?:  Area;              // graph width and height
}

export class Canvas extends SVGElem {
    /** 
     * Defines default values for all configurable parameters in `Graph`.
     * See {@link Graph.Graph.makeConfig Graph.makeConfig} for the sequence of initializations.
     * 
     * ### Configurations and Defaults
     * ```
     *  cfg.canvas = {@link Canvas.CanvasConfig <CanvasConfig>}{
     *     range: {         // the graphs background rect:
     *        w: 100,       //    width
     *        h: 100,       //    height
     *        wunit:'%',    //    unit for width
     *        hunit:'%'     //    unit for height
     *     }   
     *  } 
     * ``` 
     * @param cfg the configuration object, containing default settings for all 
     * previously configured components. 
     */
    static defaultConfig(cfg:Config) {
        cfg.canvas = <CanvasConfig>{
            range:  <Area>{ // graph width and height
                w: 100, wunit:'%',
                h: 100, hunit:'%'
            }  
        };
    }

    /**
     * Makes adjustments to cfg based on current settings
     * @param cfg the configuration object, containing default settings for all components
     */
    static adjustConfig(cfg:Config) {
    }
    
    view(node?: Vnode): Vnode {
        const cg = node.attrs.cfg;
        return m('svg', {class:'hs-graph-canvas', width:'100%', height:'100%'}, [
            this.rect({x:0, y:0},
                { w:cg.range.w, h:cg.range.h, wunit:cg.range.wunit, hunit:cg.range.hunit},
                '' // style string
            )
        ]);
    }
}

