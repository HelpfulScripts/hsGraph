/**
# hsGraph

Helpful Scripts Graph plotting functions. [Github page](https://github.com/HelpfulScripts/hsGraph)

hsGraph is a simple graphing utility written in JavaScript and based on the [Mithril](https://mithril.js.org) framework.
It supports various chart types and scales and provides a convenient programmatic configuration mechanism. 

## Usage
In mithril, simply render or mount a {@link Graph Graph} Component object and provide a `cfg`
attribute with the graph's configuration.

## Setting the data to render
 Data is provided in a rows-of-columns style array: `data[row][column]`.
 The first row in the data array contains column names by which the series can be identified.
 There is no conceptual limit to the number of rows or columns provided to `hsGraph`.
 In the configuration, 
 - set the array containing the data: `series.data = data;`  
 - and specify the x- and y-columns to render, by name: `series.series = [{xHeader:<string>, yHeader:<string>}]`

## Configuration
All graph components are highly configurable. `hsGraph` uses default values for all configurable fields 
that can easily be changed, either programmatically or via a custom stylesheet.

There are two ways to programmatically set rendering parameters.
1. change the deafult configuration style or color. This will change the default for all 
subsequently drawn graphs
```
hsgraph.Series.defaultStyle.line.width  = 10;
```
2. provide a configuration function `cfg => {}` to the `cfgFn` attribute 
when setting up the mithril mount call - see example below. The `cfgFn` 
receives a configuration object that is fully initialized with default values, 
and should overwrite parameters as needed. See the overview for each component 
for configurable parameters. Example:
```
* cfg => {
*     ...
*     cfg.series.series[0].style.line.width = 10;
* }
```



Available configuration options and their default settings are documented in:
- &nbsp; {@link Graph.Graph.defaultConfig Graph.defaultConfig }
- &nbsp; {@link Canvas.Canvas.defaultConfig Canvas.defaultConfig}
- &nbsp; {@link Chart.Chart.defaultConfig Chart.defaultConfig}
- &nbsp; {@link Axes.Axes.defaultConfig Axes.defaultConfig}
- &nbsp; {@link Grid.Grid.defaultConfig Grid.defaultConfig}
- &nbsp; {@link Series.Series.defaultConfig Series.defaultConfig}
- &nbsp; {@link Legend.Legend.defaultConfig Legend.defaultConfig}

## Graph Components
The rendered graph is organized in a layered structure of components:
- &nbsp; {@link Canvas Canvas}:  the background canvas on which all components are rendered
- &nbsp; {@link Chart Chart}: the chart area and title
- &nbsp; {@link Axes Axes}: the x- and y-axes, tick marks and labels, and axis title
- &nbsp; {@link Grid Grid}: the major and minor gridlines
- &nbsp; {@link Series Series}: the one or more data series to render
- &nbsp; {@link Legend Legend}: the legend for the shown series

## Supported Plot Types
 * <example height=640px>
 * <file name='script.js'>
 * let series = {
 *    colNames:['time', 'volume', 'costs'],
 *    rows:[[-1,  0.2, 0.3], [0.2, 0.7, 0.2], [0.4, 0.1, 0.3],
 *          [0.6, 0,   0.1], [0.8, 0.3, 0.5], [1,   0.2, 0.4]]};
 * 
 * const plot = (type, link) => m(hslayout.Layout, {
 *    rows: ['40px', 'fill'], 
 *    content: [
 *       m('h4', [m('a', {href:`#!/api/hsGraph/hsGraph.${link}`}, `${type} plot:`)]), 
 *       m(hsgraph.Graph, {cfgFn: cfg => {
 *          cfg.axes.primary.x.title.text = 'time';
 *          cfg.series.data      = [series];
 *          cfg.series.series    = [
 *              { x:'time', y:'volume', type:type },
 *              { x:'time', y:'costs', type:type }
 *          ];
 *       }}),
 * ]});
 *  
 * hsgraph.Series.defaultStyle.line.width = 6;
 * hsgraph.Series.defaultStyle.bar.width  = 20;
 * hsgraph.Series.defaultStyle.bar.offset = 25;
 * hsgraph.Series.defaultColors[0] = 'rgba(255, 0, 0, 0.5)';
 * hsgraph.Series.defaultColors[1] = 'rgba(0, 255, 0, 0.5)';
 * 
 * m.mount(root, { view:() => m('.hs-white', m(hslayout.Layout, {
 *    rows: [], content: [
 *       plot('marker', 'PlotMarkers'), 
 *       plot('line',   'PlotLine'), 
 *       plot('area',   'PlotArea'), 
 *       plot('bar',    'PlotBar')
 * ]}))});
 * </file>
 * <file name='style.css'>
 * </file>
 * </example>
*/

/** */