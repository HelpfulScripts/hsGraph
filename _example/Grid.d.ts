import { Vnode } from 'hslayout';
import { Config, VisibleCfg } from './Graph';
import { SVGElem } from './SVGElem';
export interface GridCfg extends VisibleCfg {
}
export interface GridsCfg {
    hor: GridCfg;
    ver: GridCfg;
}
export interface GridsConfig {
    major: GridsCfg;
    minor: GridsCfg;
}
export declare class Grid extends SVGElem {
    static defaultConfig(cfg: Config): void;
    static adjustConfig(cfg: Config): void;
    private drawHorGrid;
    private drawVerGrid;
    view(node?: Vnode): Vnode;
}
