import { Vnode } from 'hslayout';
import { Data } from 'hsdatab';
import { AxesConfig, Scales } from './AxesTypes';
import { CanvasConfig } from './Canvas';
import { SeriesConfig } from './Series';
import { ChartConfig } from './Chart';
import { GridsConfig } from './Grid';
import { LegendConfig } from './Legend';
import { SVGElem, TextElem, Rect } from './SVGElem';
export interface VisibleCfg extends GraphBaseCfg {
    visible: boolean;
}
export interface LabelCfg extends TextElem, VisibleCfg {
}
export interface GraphBaseCfg {
}
export interface Config {
    viewBox?: {
        w: number;
        h: number;
    };
    graph?: GraphConfig;
    canvas?: CanvasConfig;
    axes?: AxesConfig;
    chart?: ChartConfig;
    grid?: GridsConfig;
    series?: SeriesConfig;
    legend?: LegendConfig;
}
export interface GraphConfig extends GraphBaseCfg {
    margin: {
        top: number;
        left: number;
        bottom: number;
        right: number;
    };
    timeCond: any;
}
export interface CfgFn {
    (cfg: Config): void;
}
export declare class Graph extends SVGElem {
    private static makeConfig;
    protected static defaultConfig(cfg: Config): void;
    protected static adjustConfig(cfg: Config): void;
    private marginOffset;
    private scales;
    private createPlotArea;
    private createData;
    private createScales;
    adjustRange(plotArea: Rect, scales: Scales): void;
    adjustHeight(node?: Vnode): void;
    adjustMargins(cfg: Config): void;
    onupdate(node?: Vnode): void;
    oncreate(node?: Vnode): void;
    adjustDomains(cfg: SeriesConfig, scales: Scales, data: Data[]): void;
    view(node?: Vnode): Vnode;
}
