import { Vnode } from 'hslayout';
import { Data } from 'hsdatab';
import { XYScale } from './AxesTypes';
import { Plot } from './Plot';
import { SeriesDef } from './Series';
export declare class PlotArea extends Plot {
    plot(data: Data, series: SeriesDef, scales: XYScale, i: number, clipID: string): Vnode[];
}
