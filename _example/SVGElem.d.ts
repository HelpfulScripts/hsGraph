import { Vnode } from 'hslayout';
import { XYScale } from './AxesTypes';
import { DataRow } from 'hsdatab';
export interface Point {
    x: number;
    y: number;
    xunit?: string;
    yunit?: string;
}
export interface Rect {
    tl: Point;
    br: Point;
}
export interface ExtendedPoint extends Point {
    dx?: number;
    dy?: number;
    dxunit?: string;
    dyunit?: string;
}
export interface Area {
    w: number;
    h: number;
    wunit?: string;
    hunit?: string;
}
export interface TextElem {
    text: string;
    cssClass?: string;
    style?: string;
    x?: string;
    y?: string;
    xpos: TextHAlign;
    ypos: TextVAlign;
    hOffset: number;
    vOffset: number;
}
export declare function round(num: number): string;
export declare enum TextHAlign {
    start = "start",
    middle = "middle",
    end = "end"
}
export declare enum TextVAlign {
    top = "top",
    center = "center",
    bottom = "bottom"
}
export declare abstract class SVGElem {
    text(cfg: TextElem, text: string): Vnode;
    rect(tl: Point, area: Area, style: string, title?: string): Vnode;
    circle(c: Point, r: number, style: string, title?: string): Vnode;
    clipRect(tl: Point, area: Area, id: string): Vnode;
    line(x0: number, x1: number, y0: number, y1: number, cssClass?: string): Vnode;
    horLine(x0: number, x1: number, y: number, cssClass?: string): Vnode;
    verLine(x: number, y0: number, y1: number, cssClass?: string): Vnode;
    polyline(data: DataRow[], x: number, y: number, scales: XYScale, id: string, style?: string, title?: string): Vnode;
    polygon(dataFore: DataRow[], dataBack: DataRow[], x: number, yFore: number, yBack: number, scales: XYScale, id: string, style?: string, title?: string): Vnode;
    shape(points: DataRow[], id: string, style: string, title?: string): Vnode;
}
