import { Vnode } from 'hslayout';
import { Config, VisibleCfg } from './Graph';
import { DataSet } from 'hsdatab';
import { Condition } from 'hsdatab';
import { SVGElem } from './SVGElem';
import { XYScale } from './AxesTypes';
import { PlotLine } from './PlotLine';
import { PlotMarkers } from './PlotMarkers';
import { PlotBar } from './PlotBar';
import { PlotArea } from './PlotArea';
export declare class Series extends SVGElem {
    static marker: {
        circle: symbol;
        square: symbol;
        diamond: symbol;
        upTriangle: symbol;
        downTriangle: symbol;
    };
    static plot: {
        line: PlotLine;
        marker: PlotMarkers;
        bar: PlotBar;
        area: PlotArea;
    };
    static map: {
        stacked: string;
        shared: string;
    };
    static defaultConfig(cfg: Config): void;
    static adjustConfig(cfg: Config): void;
    drawClipRect(clipID: string, scales: XYScale): any;
    view(node?: Vnode): Vnode;
}
export interface ColoredCfg extends VisibleCfg {
    color: string;
}
export interface LineStyle extends ColoredCfg {
    width: number;
}
export interface MarkerStyle extends ColoredCfg {
    size: number;
    shape: Symbol;
}
export interface FillStyle extends ColoredCfg {
}
export interface TextStyle extends ColoredCfg {
}
export interface BarStyle extends ColoredCfg {
    width: number;
    offset: number;
}
export interface SeriesStyle {
    line: LineStyle;
    marker: MarkerStyle;
    fill: FillStyle;
    bar: BarStyle;
    label: TextStyle;
}
export interface SeriesDef {
    x: string;
    y?: string;
    yBase?: string;
    ySum?: string;
    l?: string;
    hOffset?: number;
    vOffset?: number;
    map?: string;
    dataIndex?: number;
    type?: string;
    style?: SeriesStyle;
    cond?: Condition;
}
export declare class SeriesConfig {
    private seriesDefs;
    data: DataSet[];
    clip: boolean;
    defaultStyle: SeriesStyle;
    defaultColors: string[];
    series: SeriesDef[];
}
