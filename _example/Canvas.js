import { m } from 'hslayout';
import { SVGElem } from './SVGElem';
export class Canvas extends SVGElem {
    static defaultConfig(cfg) {
        cfg.canvas = {
            range: {
                w: 100, wunit: '%',
                h: 100, hunit: '%'
            }
        };
    }
    static adjustConfig(cfg) {
    }
    view(node) {
        const cg = node.attrs.cfg;
        return m('svg', { class: 'hs-graph-canvas' }, [
            this.rect({ x: 0, y: 0 }, { w: cg.range.w, h: cg.range.h, wunit: cg.range.wunit, hunit: cg.range.hunit }, '')
        ]);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ2FudmFzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vc3JjL0NhbnZhcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFlQSxPQUFPLEVBQUUsQ0FBQyxFQUFRLE1BQW1CLFVBQVUsQ0FBQztBQUNoRCxPQUFPLEVBQUUsT0FBTyxFQUFRLE1BQWEsV0FBVyxDQUFDO0FBU2pELE1BQU0sYUFBYyxTQUFRLE9BQU87SUFtQi9CLE1BQU0sQ0FBQyxhQUFhLENBQUMsR0FBVTtRQUMzQixHQUFHLENBQUMsTUFBTSxHQUFpQjtZQUN2QixLQUFLLEVBQVM7Z0JBQ1YsQ0FBQyxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUMsR0FBRztnQkFDakIsQ0FBQyxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUMsR0FBRzthQUNwQjtTQUNKLENBQUM7SUFDTixDQUFDO0lBTUQsTUFBTSxDQUFDLFlBQVksQ0FBQyxHQUFVO0lBQzlCLENBQUM7SUFFRCxJQUFJLENBQUMsSUFBWTtRQUNiLE1BQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1FBQzFCLE9BQU8sQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFDLEtBQUssRUFBRSxpQkFBaUIsRUFBQyxFQUFFO1lBQ3hDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFFLENBQUMsRUFBQyxDQUFDLEVBQUMsRUFDaEIsRUFBRSxDQUFDLEVBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEtBQUssRUFBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUMsRUFDekUsRUFBRSxDQUNMO1NBQ0osQ0FBQyxDQUFDO0lBQ1AsQ0FBQztDQUNKIn0=