import { Vnode } from 'hslayout';
import { SVGElem, Area } from './SVGElem';
import { Config, VisibleCfg } from './Graph';
export interface CanvasConfig extends VisibleCfg {
    range?: Area;
}
export declare class Canvas extends SVGElem {
    static defaultConfig(cfg: Config): void;
    static adjustConfig(cfg: Config): void;
    view(node?: Vnode): Vnode;
}
