import { Vnode } from 'hslayout';
import { Data } from 'hsdatab';
import { XYScale } from './AxesTypes';
import { Plot } from './Plot';
import { SeriesDef, SeriesStyle } from './Series';
export declare class PlotBar extends Plot {
    drawBar(clipID: string, data: Data, x: number, y: number, y0: number, scales: XYScale, sStyle: SeriesStyle, s: number): any;
    setDefaults(data: Data, series: SeriesDef, scales: XYScale): void;
    plot(data: Data, series: SeriesDef, scales: XYScale, i: number, clipID: string): Vnode[];
}
