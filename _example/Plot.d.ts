import { Vnode } from 'hslayout';
import { SVGElem } from './SVGElem';
import { Data, DataRow } from 'hsdatab';
import { XYScale } from './AxesTypes';
import { SeriesStyle, SeriesDef } from './Series';
export declare abstract class Plot extends SVGElem {
    drawLine(clipID: string, data: DataRow[], x: number, y: number, scales: XYScale, sStyle: SeriesStyle, title?: string): any;
    drawMarker(clipID: string, data: DataRow[], x: number, y: number, scales: XYScale, sStyle: SeriesStyle, title?: string): any;
    drawLabel(clipID: string, data: DataRow[], x: number, y: number, lbl: number, scales: XYScale, sDef: SeriesDef): any;
    drawArea(clipID: string, data: DataRow[], x: number, yFore: number, yBack: number, scales: XYScale, sStyle: SeriesStyle, title: string): any;
    abstract plot(data: Data, series: SeriesDef, scales: XYScale, i: number, clipID: string): Vnode[];
    setDefaults(data: Data, series: SeriesDef, scales: XYScale): void;
}
