import { VisibleCfg, LabelCfg } from './Graph';
import { Scale } from './Scale';
import { Domain } from 'hsdatab';
export interface XYScale {
    x: Scale;
    y: Scale;
}
export interface Scales {
    primary: XYScale;
    secondary: XYScale;
}
export interface ScaleCfg {
    type: string;
    domain: Domain;
}
export interface Ticks {
    major: TickDefs;
    minor: TickDefs;
}
export interface TickLabel {
    pos: number;
    text: string;
}
export interface TickDefs {
    marks: number[];
    labels: TickLabel[];
}
export interface MarkCfg extends VisibleCfg {
    length: number;
}
export interface TickStruct {
    marks: MarkCfg;
    labels: LabelCfg;
    labelFmt: string;
}
export interface TicksCfg {
    major: TickStruct;
    minor: TickStruct;
}
export interface AxesConfig {
    primary: {
        x: AxisCfg;
        y: AxisCfg;
    };
    secondary: {
        x: AxisCfg;
        y: AxisCfg;
    };
}
export interface AxisCfg extends VisibleCfg {
    title: LabelCfg;
    crossesAt: number | string;
    scale: ScaleCfg;
    ticks: TicksCfg;
}
