import { m } from 'hslayout';
import { Plot } from './Plot';
export class PlotBar extends Plot {
    drawBar(clipID, data, x, y, y0, scales, sStyle, s) {
        const style = `fill: ${sStyle.bar.color};`;
        const index = (x === undefined);
        const domain = scales.x.domain();
        const offset = s * sStyle.bar.offset * (domain[1] - domain[0]) / (100 * data.getData().length);
        const width = sStyle.bar.width * (domain[1] - domain[0]) / (100 * data.getData().length);
        return m('svg', { class: 'hs-graph-series-bars' }, data.getData().map((p, i) => {
            const rx0 = scales.x.convert((index ? i : p[x]) + offset - width / 2);
            const rx1 = scales.x.convert((index ? i : p[x]) + offset + width / 2);
            const ry0 = scales.y.convert(y0 === undefined ? 0 : p[y0]);
            const ry = scales.y.convert(p[y]);
            return this.rect({ x: rx0, y: ry0 }, { h: ry - ry0, w: rx1 - rx0 }, style);
        }));
    }
    setDefaults(data, series, scales) {
        super.setDefaults(data, series, scales);
        let dom = scales.y.domain();
        if (dom[0] > 0) {
            dom[0] = 0;
            scales.y.domain(dom);
        }
        if (series.x === undefined) {
            scales.x.domain([-0.5, data.getData().length - 0.5]);
        }
    }
    plot(data, series, scales, i, clipID) {
        const x = data.colNumber(series.x);
        const y = data.colNumber(series.y);
        const yBase = series.yBase ? data.colNumber(series.yBase) : undefined;
        if (y === undefined) {
            return m('.error', '');
        }
        return [
            this.drawBar(clipID, data, x, y, yBase, scales, series.style, i),
        ];
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGxvdEJhci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9QbG90QmFyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWlEQSxPQUFPLEVBQUUsQ0FBQyxFQUFRLE1BQVcsVUFBVSxDQUFDO0FBSXhDLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBYyxRQUFRLENBQUM7QUFJdEMsTUFBTSxjQUFlLFNBQVEsSUFBSTtJQUM3QixPQUFPLENBQUMsTUFBYSxFQUFFLElBQVMsRUFBRSxDQUFRLEVBQUUsQ0FBUSxFQUFFLEVBQVMsRUFBRSxNQUFjLEVBQUUsTUFBa0IsRUFBRSxDQUFRO1FBQ3pHLE1BQU0sS0FBSyxHQUFHLFNBQVMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEdBQUcsQ0FBQztRQUMzQyxNQUFNLEtBQUssR0FBRyxDQUFDLENBQUMsS0FBSyxTQUFTLENBQUMsQ0FBQztRQUNoQyxNQUFNLE1BQU0sR0FBYyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzVDLE1BQU0sTUFBTSxHQUFHLENBQUMsR0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDNUYsTUFBTSxLQUFLLEdBQUksTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEdBQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUUsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzVGLE9BQU8sQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFDLEtBQUssRUFBQyxzQkFBc0IsRUFBQyxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxHQUFHLENBQzlELENBQUMsQ0FBVSxFQUFFLENBQVEsRUFBRSxFQUFFO1lBQ3JCLE1BQU0sR0FBRyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sR0FBRyxLQUFLLEdBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbkUsTUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxHQUFHLEtBQUssR0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRSxNQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEtBQUcsU0FBUyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3hELE1BQU0sRUFBRSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFDLENBQUMsRUFBQyxHQUFHLEVBQUUsQ0FBQyxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsQ0FBQyxFQUFDLEVBQUUsR0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFDLEdBQUcsR0FBQyxHQUFHLEVBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNuRSxDQUFDLENBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQztJQUVELFdBQVcsQ0FBQyxJQUFTLEVBQUUsTUFBZ0IsRUFBRSxNQUFjO1FBQ25ELEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN4QyxJQUFLLEdBQUcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzdCLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUNaLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDWCxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN4QjtRQUNELElBQUksTUFBTSxDQUFDLENBQUMsS0FBSyxTQUFTLEVBQUU7WUFDeEIsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsTUFBTSxHQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDdEQ7SUFDTCxDQUFDO0lBRUQsSUFBSSxDQUFDLElBQVMsRUFBRSxNQUFnQixFQUFFLE1BQWMsRUFBRSxDQUFRLEVBQUUsTUFBYTtRQUNyRSxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuQyxNQUFNLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFBLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO1FBQ3JFLElBQUksQ0FBQyxLQUFHLFNBQVMsRUFBRTtZQUFFLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBQyxFQUFFLENBQUMsQ0FBQztTQUFFO1FBQzdDLE9BQU87WUFDSCxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO1NBQ25FLENBQUM7SUFDTixDQUFDO0NBQ0oifQ==