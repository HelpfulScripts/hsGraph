import { m } from 'hslayout';
import { SVGElem } from './SVGElem';
export class Grid extends SVGElem {
    static defaultConfig(cfg) {
        cfg.grid = {
            major: {
                hor: { visible: true },
                ver: { visible: true }
            },
            minor: {
                hor: { visible: false },
                ver: { visible: false }
            }
        };
    }
    static adjustConfig(cfg) {
    }
    drawHorGrid(cfg, scale, range, ticks) {
        return !cfg.visible ? m('svg') : m('svg', { class: 'hs-graph-grid-hor' }, ticks.marks.map((t) => this.horLine(range[0], range[1], scale.convert(t))));
    }
    drawVerGrid(cfg, scale, range, ticks) {
        return !cfg.visible ? m('svg') : m('svg', { class: 'hs-graph-grid-ver' }, ticks.marks.map((t) => this.verLine(scale.convert(t), range[0], range[1])));
    }
    view(node) {
        const cfg = node.attrs.cfg;
        const scales = node.attrs.scales;
        const ps = scales.primary;
        return m('svg', { class: 'hs-graph-grid' }, [
            m('svg', { class: 'hs-graph-grid-minor' }, [
                this.drawHorGrid(cfg.minor.hor, ps.y, ps.x.range(), ps.y.ticks().minor),
                this.drawVerGrid(cfg.minor.ver, ps.x, ps.y.range(), ps.x.ticks().minor)
            ]),
            m('svg', { class: 'hs-graph-grid-major' }, [
                this.drawHorGrid(cfg.major.hor, ps.y, ps.x.range(), ps.y.ticks().major),
                this.drawVerGrid(cfg.major.ver, ps.x, ps.y.range(), ps.x.ticks().major)
            ])
        ]);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiR3JpZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9HcmlkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWlCQSxPQUFPLEVBQUUsQ0FBQyxFQUFRLE1BQVcsVUFBVSxDQUFDO0FBR3hDLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBVyxXQUFXLENBQUM7QUF5QnpDLE1BQU0sV0FBWSxTQUFRLE9BQU87SUFxQjdCLE1BQU0sQ0FBQyxhQUFhLENBQUMsR0FBVTtRQUMzQixHQUFHLENBQUMsSUFBSSxHQUFnQjtZQUNwQixLQUFLLEVBQUU7Z0JBQ0gsR0FBRyxFQUFFLEVBQUUsT0FBTyxFQUFDLElBQUksRUFBRTtnQkFDckIsR0FBRyxFQUFFLEVBQUUsT0FBTyxFQUFDLElBQUksRUFBRTthQUN4QjtZQUNELEtBQUssRUFBRTtnQkFDSCxHQUFHLEVBQUUsRUFBRSxPQUFPLEVBQUMsS0FBSyxFQUFFO2dCQUN0QixHQUFHLEVBQUUsRUFBRSxPQUFPLEVBQUMsS0FBSyxFQUFFO2FBQ3pCO1NBQ0osQ0FBQztJQUNOLENBQUM7SUFNRCxNQUFNLENBQUMsWUFBWSxDQUFDLEdBQVU7SUFDOUIsQ0FBQztJQUtPLFdBQVcsQ0FBQyxHQUFxQixFQUFFLEtBQVcsRUFBRSxLQUFjLEVBQUUsS0FBYztRQUNsRixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFDLG1CQUFtQixFQUFFLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUMxRixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUNyRCxDQUFDLENBQUM7SUFDUCxDQUFDO0lBS08sV0FBVyxDQUFDLEdBQXFCLEVBQUUsS0FBVyxFQUFFLEtBQWMsRUFBRSxLQUFjO1FBQ2xGLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxLQUFLLEVBQUMsbUJBQW1CLEVBQUUsRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQzFGLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQ3JELENBQUMsQ0FBQztJQUNQLENBQUM7SUFHRCxJQUFJLENBQUMsSUFBWTtRQUNiLE1BQU0sR0FBRyxHQUFlLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1FBQ3ZDLE1BQU0sTUFBTSxHQUFVLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQ3hDLE1BQU0sRUFBRSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUM7UUFDMUIsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFDLGVBQWUsRUFBQyxFQUFFO1lBQ3RDLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxLQUFLLEVBQUMscUJBQXFCLEVBQUUsRUFBRTtnQkFDdEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUM7Z0JBQ3ZFLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDO2FBQzFFLENBQUM7WUFDRixDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFDLHFCQUFxQixFQUFFLEVBQUU7Z0JBQ3RDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDO2dCQUN2RSxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQzthQUMxRSxDQUFDO1NBQ0wsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztDQUNKIn0=