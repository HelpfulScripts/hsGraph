import { Vnode } from 'hslayout';
import { Config, LabelCfg, VisibleCfg } from './Graph';
import { SVGElem, Rect } from './SVGElem';
export interface ChartConfig extends VisibleCfg {
    title: LabelCfg;
}
export declare class Chart extends SVGElem {
    static defaultConfig(cfg: Config): Config;
    static adjustConfig(cfg: Config): void;
    static clientWidth: number;
    static clientHeight: number;
    onupdate(node?: Vnode): void;
    updateTitleSize(node: Vnode): void;
    drawBackground(plotArea: Rect): any;
    drawTitle(plotArea: Rect, cfg: ChartConfig): any;
    view(node?: Vnode): Vnode;
}
