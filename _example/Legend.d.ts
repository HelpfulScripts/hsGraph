import { Vnode } from 'hslayout';
import { Config } from './Graph';
export interface LegendConfig {
}
export declare class Legend {
    static defaultConfig(cfg: Config): void;
    static adjustConfig(cfg: Config): void;
    view(node?: Vnode): Vnode;
}
