import { Vnode } from 'hslayout';
import { XYScale, AxesConfig, TickStruct, TickDefs } from './AxesTypes';
import { Config, LabelCfg } from './Graph';
import { Scale } from './Scale';
import { SVGElem, Area } from './SVGElem';
export declare class Axes extends SVGElem {
    static type: {
        linear: string;
        log: string;
        date: string;
        index: string;
        percent: string;
        ordinal: string;
        nominal: string;
    };
    static defaultConfig(cfg: Config): void;
    static adjustConfig(cfg: Config): void;
    drawAxisLine(x: boolean, range: Area, cross: number): any;
    drawTitle(x: boolean, ttlCfg: LabelCfg, type: string, range: Area, cross: number): Vnode;
    drawTickMarks(x: boolean, type: string, crossesAt: number, scale: Scale, ticks: TickDefs, cfg: TickStruct): Vnode;
    drawTickLabels(x: boolean, type: string, crossesAt: number, scale: Scale, ticks: TickDefs, cfg: TickStruct): Vnode;
    drawAxis(dir: string, scales: XYScale, type: string, axisCfg: AxesConfig): Vnode;
    view(node?: Vnode): Vnode;
}
