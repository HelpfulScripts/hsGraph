import { Domain, NumRange } from 'hsdatab';
import { Ticks, ScaleCfg } from './AxesTypes';
export declare class Scale {
    private cfg;
    private typeVal;
    private rangeVal;
    private domVal;
    private domMinAuto;
    private domMaxAuto;
    private labelFmt;
    constructor(cfg: ScaleCfg);
    setLabelFormat(labelFmt: string): void;
    range(r?: NumRange): NumRange;
    domain(dom?: Domain): Domain;
    scaleType(s?: string): string;
    setAutoDomain(dom: NumRange): void;
    ticks(numTicks?: number): Ticks;
    convert(domVal: number): number;
}
